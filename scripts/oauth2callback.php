<?php
// Example code is used from https://developers.google.com/api-client-library/php/auth/web-app
// with some modification.

// Process to include Google's google-api-php-client/src folder that is included locally.
// Absolute paths are being generated.
// The join function is necessary to generate a path that OS independent
require_once join(DIRECTORY_SEPARATOR, array($_SERVER['DOCUMENT_ROOT'], 'scripts', 'shared.php'));
require_once $GLOBALS["AUTOLOAD_PATH"];
set_include_path($GLOBALS["GOOGLE_PATH"]);
session_start();

$client = new Google_Client();
$client->setAuthConfigFile($GLOBALS["CONFIG_PATH"]);

// Setting up the redirect to be back to this file because this file is also
// responsible for checking if the user has authenticated.
$client->setRedirectUri($GLOBALS["CALLBACK_URL"]);

// Specifies which type of Google Service we would like to access.
// Current scope allow only reading of subscriptions and channel information.
$client->addScope(Google_Service_YouTube::YOUTUBE_READONLY);

if (! isset($_GET['code'])) {
    $auth_url = $client->createAuthUrl();
    header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
}
else {
    $client->authenticate($_GET['code']);
    $_SESSION['access_token'] = $client->getAccessToken();
    $redirect_uri = $GLOBALS["INDEX_URL"];
    header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}
