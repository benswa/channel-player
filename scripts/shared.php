<?php
// This is the place that stores superglobal. The superglobal variables are storing the absolute paths
// to the files. The absolute paths should only be used internally and should _never_ be exposed as an
// URL for security reasons unless the variable is storing an URL address and not an absolute path.

$GLOBALS["INDEX_PATH"] = join(DIRECTORY_SEPARATOR, array($_SERVER['DOCUMENT_ROOT'], 'index.php'));
$GLOBALS["JUMBOTRON_PATH"] = join(DIRECTORY_SEPARATOR, array($_SERVER['DOCUMENT_ROOT'], 'scripts', 'jumbotron.php'));
$GLOBALS["API_PATH"] = join(DIRECTORY_SEPARATOR, array($_SERVER['DOCUMENT_ROOT'], 'scripts', 'api.php'));
$GLOBALS["CONNECT_PATH"] = join(DIRECTORY_SEPARATOR, array($_SERVER['DOCUMENT_ROOT'], 'scripts', 'connect.php'));
$GLOBALS["CONFIG_PATH"] = join(DIRECTORY_SEPARATOR, array($_SERVER['DOCUMENT_ROOT'], 'config.json'));

// This is the source paths to the folder src and to the file autoload.php
$SRC_PATH = join(DIRECTORY_SEPARATOR, array($_SERVER['DOCUMENT_ROOT'], 'google-api-php-client', 'src'));
$GLOBALS["GOOGLE_PATH"] = $SRC_PATH;
$GLOBALS["AUTOLOAD_PATH"] = join(DIRECTORY_SEPARATOR, array($SRC_PATH, 'Google', 'autoload.php'));

// This url must be encoded exactly as is for it to work with google's api servers. This variable is used
// specifically as an URL.
$GLOBALS["CALLBACK_URL"] = "https://" . join(DIRECTORY_SEPARATOR, array($_SERVER['HTTP_HOST'], 'scripts',
        'oauth2callback.php'));

$GLOBALS["INDEX_URL"] = 'https://' . $_SERVER['HTTP_HOST'] . '/';