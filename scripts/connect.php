<?php
// This file is used to connect to a mysql database by first reading the config
// in the scripts directory. The db_config.json file is expected to be in the same
// directory as this connect.php file.

session_start();

function get_config() {
  $db_config_file = join(DIRECTORY_SEPARATOR, array($_SERVER['DOCUMENT_ROOT'], 'scripts', 'db_config.json'));
  $db_config = file_get_contents($db_config_file);
  $db_config_array = json_decode($db_config, true);

  return $db_config_array;
}

function db_connect() {
  $config = get_config();

  try {
    // http://wiki.hashphp.org/PDO_Tutorial_for_MySQL_Developers
    $db = new PDO (
      "mysql:host=" . $config['DB_HOST'] . ";dbname=" . $config['DB_NAME'],
      $config['USERNAME'],
      $config['PASSWORD'],
      array(PDO::ATTR_EMULATE_PREPARES => false, PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8')
      );
  }
  catch (PDOException $err) {
    echo "Failed: " . $err->getMessage() . "<br>" . PHP_EOL;
  }

  return $db;
}

function db_check_table($db) {
  // http://php.net/manual/en/ref.pdo-mysql.php
  // Youtube user id also seems to be just the channel id without the UC in front
  // of the string, but we will use the channel id as user identifier.
  $youtube_user =
 "CREATE TABLE IF NOT EXISTS youtube_user (
  user_channel_id VARCHAR(24) NOT NULL,
  CONSTRAINT pk_User_Id PRIMARY KEY (user_channel_id)
  )
 ";

 // subscription_id is the channel id the user is subscribed to
 $user_subscriptions =
 "CREATE TABLE IF NOT EXISTS user_subscriptions (
  subscription_channel_id VARCHAR(24) NOT NULL,
  subscription_upload_id VARCHAR(24) NOT NULL,
  subscription_unread INTEGER NOT NULL DEFAULT 0,
  user_channel_id VARCHAR(24) NOT NULL,
  CONSTRAINT pk_sub_chan_id PRIMARY KEY (subscription_channel_id),
  CONSTRAINT pk_user_id FOREIGN KEY (user_channel_id) REFERENCES youtube_user (user_channel_id),
  CONSTRAINT chk_sub_unread CHECK (subscription_unread >= 0)
  )
 ";

  // Making the queries here to create the tables
  $rc = $db->exec($youtube_user);
  $rc = $db->exec($user_subscriptions);
}