<?php
require_once join(DIRECTORY_SEPARATOR, array($_SERVER['DOCUMENT_ROOT'], 'scripts', 'shared.php'));
require_once $GLOBALS["AUTOLOAD_PATH"];
set_include_path($GLOBALS["GOOGLE_PATH"]);
session_start();


function setGoogleClient() {
    // Creates a new google client, configures it, and returns it
    $client = new Google_Client();
    $client->setApplicationName("Channel Player");
    $client->setAuthConfigFile($GLOBALS["CONFIG_PATH"]);

    // Allow only reading of subscriptions and channel information
    $client->addScope(Google_Service_YouTube::YOUTUBE_READONLY);

    return $client;
}

function setYoutubeService($client) {
    // If the user does not have an access_token, it means that the user has not authenticated
    // to access their information. This function returns a Google_Service_Youtube object or will
    // return null if user is not authenticated.

    if (!(isset($_SESSION['access_token']))) {
        // Makes sure that the user has a token
        echo "setYoutubeSubService() error. Not authorized.";
        return null;
    }

    // If the user is authenticated.
    $youtubeService = new Google_Service_YouTube($client);

    return $youtubeService;
}

function getSubscriptionList($youtube) {
    // This function attempts to get the user's youtube subscriptions. If
    // for any reason the the function is used inappropriately or there is
    // some kind of error in retrieving in the response, the function returns
    // an empty array.
    //
    // The data structure of the subscription list is a 2D array where the
    // subscription channel id is the key and points to another array.

    if (!(isset($_SESSION['access_token']))) {
        // Makes sure that the user has a token
        echo "getSubscriptionList() error. Not authorized.";
        return null;
    }
    if (!isset($youtube)) {
        echo "getSubscriptionList() error. youtube was not set.";
        return null;
    }

    // Initially the string is empty since the program has not made a request yet for a list of subscriptions
    $pageToken = '';
    $response = null;
    $subList = array();

    while (true) {
        $response = $youtube->subscriptions->listSubscriptions('contentDetails, snippet', array('mine' => 'true',
            'maxResults' => '50', 'order' => 'unread', 'pageToken' => $pageToken));

        foreach ($response['items'] as &$item) {
            // Going through each item from the response to retrieve the subscription information from
            // the data structure.
            $channelId = $item['snippet']['resourceId']['channelId'];
            $new_item_count = $item['contentDetails']['newItemCount'];

            // Store the subscribed information into $subList
            $subList[$channelId]['unread'] = $new_item_count;
            //echo "</br>".$channelId." : ".$subList[$channelId]['unread'].PHP_EOL; ///////Debug
        }
        
        if (!isset($response['nextPageToken'])) {
            // Breaks out of the loop since there are no more subscriptions to process
            break;
        }
        else {
            // If there are more subscriptions, we start set $pageToken so that we can
            // request the next 50 results.
            $pageToken = $response['nextPageToken'];
        }
    }

    return $subList;
}

function getUploadsIdList($youtube, &$subList) {
    // This function takes the reference of an array of channelIds that the function
    // can use to request the upload playlist ids. Realistically, we can just manipulate
    // the channelIds to get the uploadId and this would save a whole bunch of quotas.
    //
    // Current difference between all ChannelId and the UploadsId is currently one letter from UC... to UU...
    // Example ChannelId: UC4w1YQAJMWOz4qtxinq55LQ
    // Example UploadsId: UU4w1YQAJMWOz4qtxinq55LQ

    if (!(isset($_SESSION['access_token']))) {
        // Makes sure that the user has a token
        echo "getUploadsIdList() error. Not authorized.";
        return null;
    }
    if (!isset($youtube)) {
        echo "getUploadsIdList() error. youtube was not set.";
        return null;
    }

    // Initially the string is empty since the program has not made a request yet for a list of subscriptions
    $pageToken = '';
    $response = null;
    $channelIdString = implode(", ", array_keys($subList));

    while (true) {
        $response = $youtube->channels->listChannels('contentDetails', array('maxResults' => '50',
            'id' => $channelIdString, 'pageToken' => $pageToken));

        foreach ($response['items'] as &$item) {
            // Going through each item from the response and retrieving information from the data structure.
            $channelId = $item['id'];
            $uploadsId = $item['contentDetails']['relatedPlaylists']['uploads'];

            // Store the subscribed information into $subList
            $subList[$channelId]['uploadId'] = $uploadsId;
            //echo "</br>".$channelId." : ".$subList[$channelId]['uploadId'].PHP_EOL; ///////Debug
        }
        if (!isset($response['nextPageToken'])) {
            // Breaks out of the loop since there are no more subscriptions to process
            break;
        }
        else {
            // If there are more subscriptions, we start set $pageToken so that we can
            // request the next 50 results.
            $pageToken = $response['nextPageToken'];
        }
    }
}

function getVideoIds($youtube, &$subList) {
//function getVideoIds($youtube, $uploadIdArray) {
    // This function takes the reference of an array of channelIds whose subarray contains the uploadIds
    // that the function can use to request the videoIds that we care about. The $subList is especially
    // useful because it also contains the number of unread/unwatched videos so we can request just right
    // number of videoIds we need.
    //
    // Current difference between all ChannelId and the UploadsId is currently one letter from UC... to UU...
    // Example ChannelId: UC4w1YQAJMWOz4qtxinq55LQ
    // Example UploadsId: UU4w1YQAJMWOz4qtxinq55LQ

    if (!(isset($_SESSION['access_token']))) {
        // Makes sure that the user has a token
        echo "getUploadsIdList() error. Not authorized.";
        return null;
    }
    if (!isset($youtube)) {
        echo "getUploadsIdList() error. youtube was not set.";
        return null;
    }

    // Initially the string is empty since the program has not made a request yet for a list of subscriptions
    $pageToken = '';
    $response = null;
    $remaining_unread = 0;

    foreach ($subList as $key => $subArray) {
        $remaining_unread = $subArray['unread'];
        $subList[$key]['videoId'] = array();
        
        if ($remaining_unread <= 0) {
            // If the current channelId contains no unwatched videos, skip
            continue;
        }

        while($remaining_unread) {
            if ($remaining_unread > 50) {
                // Grab 50 if there are more than 50 unread videos
                $response = $youtube->playlistItems->listPlaylistItems('contentDetails',
                            array('maxResults' => 50,
                            'playlistId' => $subArray['uploadId'], 'pageToken' => $pageToken));
                
                // If there are more subscriptions, we start set $pageToken so that we can
                // request the next 50 results.
                $remaining_unread -= 50;
                $pageToken = $response['nextPageToken'];
            }
            else {
                // Grab just the right amount if there are less than unread videos
                $response = $youtube->playlistItems->listPlaylistItems('contentDetails',
                                array('maxResults' => $remaining_unread,
                                'playlistId' => $subArray['uploadId'], 'pageToken' => $pageToken));
                $remaining_unread = 0;
                $pageToken = '';
            }
            
            foreach ($response['items'] as &$item) {
                // Going through each item from the response and retrieving information from the data structure.
                $videoId = $item['contentDetails']['videoId'];

                // Store the subscribed information into the videoId subarray
                array_push($subList[$key]['videoId'], $videoId);
                //echo "</br>".$key." : ".$videoId.PHP_EOL; ///////Debug
            }
        }
    }
}