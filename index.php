<?php
// Example code is used from https://developers.google.com/api-client-library/php/auth/web-app
// with some modification.

// Process to include Google's google-api-php-client/src folder that is included locally.
// Absolute paths are being generated.
// The join function is necessary to generate a path that OS independent.
require_once join(DIRECTORY_SEPARATOR, array($_SERVER['DOCUMENT_ROOT'], 'scripts', 'shared.php'));
require_once $GLOBALS["AUTOLOAD_PATH"];
require_once $GLOBALS["API_PATH"];
require_once $GLOBALS["CONNECT_PATH"];
set_include_path($GLOBALS["GOOGLE_PATH"]);
$JUMBOTRON_PATH = $GLOBALS["JUMBOTRON_PATH"];

session_start();
/*
$client = setGoogleClient();
if (isset($_SESSION['access_token'])) {
  $client->setAccessToken($_SESSION['access_token']);
  $youtube = setYoutubeService($client);
  if (isset($youtube)) {
    // Generates an array of the user's subscriptions
    $subList = getSubscriptionList($youtube);

    // Gets the subscriptions' upload playlist named: uploadId
    getUploadsIdList($youtube, $subList);

    // Gets the videoIds of the unread videos from the subscriptions.
    // It is stored in an subarray named 'videoId'
    // The subarray is equal to $subList[$channelId]['videoId']
    getVideoIds($youtube, $subList);
  }
}
*/

$db = db_connect();
db_check_table($db);
?>

<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Channel Player</title>
    <meta name="description" content="Watch youtube videos like TV.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.2.0/foundation-flex.min.css"/>
    <link rel="stylesheet" href="css/app.css"/>
  </head>

  <body>
    <div class="row">
      <div class="large-12 columns">
        <div class="callout large">
          <h3>Watch youtube videos in no time!</h3>
          <p><a href="<?php echo $GLOBALS["CALLBACK_URL"]; ?>" class="medium button">Sign In</a></p>
        </div>
      </div>
    </div>
    
    <div class="row align-center">
  <!--
  <?php /*foreach ($subList as $channelId => $subArray) {
          foreach ($subArray['videoId'] as $videoId) { ?>
      <div class="large-6 medium-6 small-6 columns align-center">
        <div class="flex-video">
          <iframe class="youtube" id="<?php echo $videoId; ?>" frameborder="0" allowfullscreen
                  src="https://www.youtube.com/embed/<?php echo $videoId; ?>"></iframe>
        </div>
      </div>
      </br>
  <?php echo PHP_EOL; }}*/ ?>
  -->
      <div class="small-6 medium-6 large-6 columns">
        <div class="flex-video">
          <iframe id="TtOqqj7RhZY" frameborder="0" allowfullscreen
                  src="https://www.youtube.com/embed/TtOqqj7RhZY"></iframe>
        </div>
      </div>
    </div>

    <footer>
      <p>&copy; superflux <?php echo date("Y"); ?></p>
    </footer>

    <!-- Importing libraries and loads local copies if CDN (Content Delivery Network) fails -->
    <!-- http://eddmann.com/posts/providing-local-js-and-css-resources-for-cdn-fallbacks/ -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <script>
      // Checks jquery-2.2.2.min.js is loaded.
      window.jQuery || document.write('<script src="js/vendor/jquery.min.js"><\/script>');
    </script>
    <script src="js/vendor/what-input.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/foundation/6.2.0/foundation.min.js"></script>
    <script src="js/app.js"></script>
  </body>
</html>
